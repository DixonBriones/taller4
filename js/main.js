/* Crear un script que solicite un valor numérico al usuario en
 base octal (8) y posteriormente muestre su equivalente en base decimal
 (10). Muestre el resultado en una ventana emergente.*/

var form_convertidor= document.getElementById("form_convertidor");
var octal = document.getElementById("num_octal");

form_convertidor.addEventListener("submit", e=>{
    e.preventDefault();
    var octalverificador=/^[0-7]+$/;

    if(octalverificador.test(octal.value)){
        var decimal = parseInt(octal.value, 8)
        alert("El resultado del numero octal " + octal.value + " en decimal es: " + decimal) ; 
    }else{
        alert("Debe ingresar un numero de base 8");
    };

})

/*
Crear una aplicación que solicite dos números enteros al usuario.
 estos números serán los parámetros de la función que se debe
 definir y que devolverá la suma, resta, multiplicación y división
 de dichos números. Utilice el método alert() para mostrar el resultado por
  pantalla. es necesario que recuerde el uso del método parseInt() para
   controlar los datos que ingresa el usuario.
 */
function calcular_num(){
    var a = parseInt(document.getElementById("num1").value);
    var b = parseInt(document.getElementById("num2").value);
    resultados(a,b)
}

function resultados(num_1, num_2){
    var suma= num_1+num_2;
    var resta=num_1-num_2;
    var multiplicación=num_1*num_2;
    var division=num_1/num_2;
    alert("Suma: "+suma+"\n Resta: " +resta+ "\n Multiplicacion: " +multiplicación + "\n Division: " +division)
}